+++
title = " Home Page "
description = " This will provide an overview of what you will see on this site. "
+++

<br>
</br>
<p style = "text-align: center;">
<i> For IDS 721, one of the unique aspects we will learn is replying on systems that are not local to your own computer but rather leveraging resources within the cloud. This website is a perfect demonstration of that as Zola is a SaaS (Software-as-a-Service). For this website, the aim to catalog not only previous key projects I have completed but continuously add to my project portfolio throughout the semester. </i>
</p>