+++
title = "Song Recommendation System"
description = "This Github Repo supports a song recommendation system that is built by data pulled from Kaggle via Spotify API. The recommendation system enables users to get suggestions based on Popularity of the music/song"
date = 2024-01-27
+++

<h2>
<b>
Data
</b>
</h2>
The dataset used for this project is from Kaggle and can be found here: 
Spotify dataset extracted by VATSAL MAVANI and posted on Kaggle, link: [Dataset](https://www.kaggle.com/datasets/vatsalmavani/spotify-dataset)

**Some features of the data:**

Songs releasing years from *1921 - 2020*

Songs Count: *133,638*

Genres Count: *2,973*
<h2>
<b>
Github
</b>
</h2>
To replicate the results, please fork this git repository or clone it using `git clone git@github.com:Yer1k/Song_Recommender.git`

Upon doing so, please ensure all test files and source/code files are within the same directory. Please move all test files into your tests/directory if running this code locally. Please also install all libraries outlined within the `requirements.txt` file.
In addition to the libraries specified within the requirements file, please also `pip install pytest` before importing/executing the test files.