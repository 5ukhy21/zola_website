+++
title = "2022 FIFA World Cup Predictor"
description = "Welcome to this Totally Not Serious FIFA Outcome Prediction Tool for the 2022 World Cup (otherwise known as TONS OF FUN)"
date = 2024-01-27
+++

This was a project for data engineering, not a statistics class. As such, the predictions are engineered to be somewhat serious, but we really used this opportunity to exercise several other things and try some new stuff. Check out the youtube demo here.

It's important to point out that this tool will work for matches outside the 2022 World Cup. If you want to simulate any matches, navigate to the Streamlit App Landing and run your own simulations!

Below is an architectural schematic of this project:
<p align = "center">
    <img src="/images/tons_of_fun.png" width = "1000"/>
</p>

<h2>
<b>
Data
</b>
</h2>
We are accessing data from two locations:

* [538's Soccer Power Index](https://projects.fivethirtyeight.com/soccer-api/international/spi_global_rankings_intl.csv) - The Soccer Power Index, or SPI, is a calculated relative strength for each international team. We stream this data live each time the app is loaded and use it as the base for many of the actions in our logic tools.

* [ESPN's World Cup Scoreboard](https://www.espn.com/soccer/scoreboard?league=fifa.world&xhr=1) - JSON source data for the scoreboard web page available on ESPN. We add the `&xhr=1` to the end of the address to access the JSON, which significantly reduces the complexity of the logic required.

