+++
title = "Accelerating Medicines Partnership Parkinson's Disease (AMP-PD) Progression Prediction"
description = "Analysis of Protein and Peptide Levels Over Time to Predict Progression of Parkinson’s Disease"
date = 2024-01-27
+++

<p align = "center">
    <img src="/images/parkinsons_disease_image.jpeg" width = "400"/>
</p>

Parkinson's Disease is a neurodegenerative disorder that affects millions of people worldwide. The progression of the disease is commonly measured using the Movement Disorder Society - Unified Parkinson's Disease Rating Scale (MDS-UPDRS) scores, which assess motor and non-motor symptoms. The Accelerating Medicines Partnership Parkinson's Disease (AMP-PD) Progression Prediction Kaggle competition seeks to discover unforeseen patterns and predict the progression of Parkinson's Disease using clinical UPDRS scores and peptide and protein abundance data captured from cerebrospinal fluid (CSF) samples. Unlike previous studies that have solely focused on statistical models and peptide/protein levels, our project and this data science competition will be unique in approach as it combines this information with clinical data and analyzes it all together, potentially contributing to a better understanding of the disease and ultimately leading to improved treatments for patients through UPDRS scores.

This study provides a detailed walkthrough of crucial assumptions made, the development of the predictive models, and the limitations for predicting UPDRS scores to determine Parkinson's Disease progression using peptide and protein abundance data from CSF samples. Analysis and findings from this data science competition can potentially lead to the development and breakthroughs of new medicines and medical techniques, such as specific pharmacotherapies, that aim to slow the progression or cure Parkinson's Disease.

<h2>
<b>
Data
</b>
</h2>
This project consisted of 3 files: clinical trial data, peptide data, and protein data. All associated training and test files are provided in the source repository 00_source_data. You can download all datasets here: <a href="https://www.kaggle.com/competitions/amp-parkinsons-disease-progression-prediction/data"> Dataset from Kaggle </a>

<h2>
<b>
Report Summary
</b>
</h2>
The report focuses on using machine learning techniques to predict the progression of Parkinson's Disease. The study involved merging three datasets, including clinical data, peptide, and protein data, into a single dataframe. The data was pre-processed using a MinMaxScaler to scale the numerical features within a range of 1s and 0s, which is more appropriate for this case than StandardScaler since it does not assume a normal distribution of the data. Using the MinMaxScalar ensured that all features were based on a similar scale and prevented any one feature from exerting a disproportionate influence on the others (p. 11). Once the features were scaled, the team used a 10-fold split to evaluate the performance of the models, in which the models were trained on 9 portions of the data and then tested on the remaining portion. This method was repeated 10 times with different evaluations before being averaged to obtain a more reliable estimate of each model’s performance (p. 11). The study identified the best machine learning model for this problem, which was the ensemble stacking model. The model achieved the lowest SMAPE of 57.2, which is essential for clinical decision-making and patient care. The report highlights the complexity of using machine learning techniques to predict the progression of Parkinson's Disease and recommends that machine learning tools be considered complementary to current medical procedures. The report also identifies several limitations and areas for future work, including seeking insights from medical professionals, resolving missing values with imputation, and partnering with foundations and organizations dedicated to finding a cure for Parkinson's Disease. Overall, the report's significance lies in its contribution to the understanding of Parkinson's Disease and the potential use of machine learning techniques to predict its progression.
<h2>
<b>
Github
</b>
</h2>
Open your terminal and run the following lines in order: 

1. `git clone git@github.com:5ukhy21/IDS-705_ML-Project_Parkinson-Disease-Analysis.git`

2. `cd 10_code`

From here you'll be able to run any of notebooks in the repository without any trouble (with the exception of changing directory of source data).

The notebook used to generated the results found from our report is `best score.ipynb`