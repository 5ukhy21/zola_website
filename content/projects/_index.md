+++
title = " Projects "
description = " Interesting projects that have been done throughout my time at MIDS. "
sort_by = "date"
paginate_by = 5
template = "my-post-section.html"
+++