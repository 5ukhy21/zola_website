+++
title = "About Me"
description = "This page provides a quick background into my life, my career, and my journey as a Duke MIDS candidate thus far."
date = 2024-01-27
+++
For those of you reading this page, I would like to take this time to introduce myself to you.
My name is Sukhpreet Sahota and I am a graduate student within the MIDS program at Duke University.
To tell you a little bit more about myself, I am a first-generation college student from my family, I am a proud Philadelphian (was born and raised outside West Philadelphia and bleed all Philly sports), am a proud graduate from Villanova University (which is located in the suburbs of Philadelphia) where I majored in Business Analytics, Management Information Systems, and Marketing, and love to run, cook, and take spin classes/ride my Peloton.


Before coming to graduate school and upon my graduation from Villanova University, I worked within the consulting profession for ~5 years. 
This opportunity, while challenging at times - and that might be putting it lightly, was extremely rewarding.
I was able to get a sense of the profession from not only the different industries I worked in but also from the lenses of my mentors, who were part of different alignments within the firm.
During this time, I found a great love and passion for using data to resolve client and firm initiative needs.
This ultimately led me to exploring the possibilities of coming back to school, and thus has landed me here at Duke University.


I look forward to using the skills gained from this experience (and particularly, this class) to understand the ever-evolving world around us that consists and consumes our daily lives with data.

<p align = "center">
    <img src="/images/SSS_Duke_pic.png" width = "400"/>
</p>
