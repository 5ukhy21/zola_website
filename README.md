# Zola Website

[![CI/CD status](https://gitlab.com/5ukhy21/zola_website/badges/main/pipeline.svg)](https://gitlab.com/5ukhy21/zola_website/badges/main/pipeline.svg)

## Access to Zola static website

[Hosted on GitLab.io](https://5ukhy21.gitlab.io/zola_website/)

[Hosted on Netlify](https://sukhpreetsahota.netlify.app)

## Demo

![](static/images/IDS_721_-_Zola_Website_Demo.mp4){ width="800" height="600" style="display:block; margin-left:auto; margin-right:auto"}

## Screenshots
This Zola website consists of 3 pages - a Home page, an About Me page, and a Projects page.

### Home Page
![Home Page](static/images/zola_website_1.png)

### About Me
![About Me](static/images/zola_website_2.png)

### Projects
![Projects](static/images/zola_website_3.png)

![](static/images/zola_website_4.png)

## Roadmap
Throughout the semester, this zola website will be continuously updated to included cloud computing projects as well as refining/updating the website features.

## Authors and acknowledgment
This zola website uses the [Blow zola theme made with Tailwindcss](https://www.getzola.org/themes/blow/), authored by Thomas Chartron. 
